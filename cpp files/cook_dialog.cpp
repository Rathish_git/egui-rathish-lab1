#include "cook_dialog.h"
#include "ui_cook_dialog.h"
#include "globals.h"
#include "recipie_dialog.h"
#include "ingred_dialog.h"

#include <string>
#include <QDebug>
#include <regex>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QMultiMap>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

cook_Dialog::cook_Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cook_Dialog)
{
    ui->setupUi(this);
}

cook_Dialog::~cook_Dialog()
{
    delete ui;
}

void cook_Dialog::on_check_recipie_pushButton_clicked()
{

    global_recipe_list.clear();

    QString names_str = ui->names_plainTextEdit->toPlainText();
    QStringList names_list = names_str.split("\n");

    names_list.removeDuplicates();

    qDebug() << names_list;


    for(int i = 0; i < names_list.count(); i++){
        if(rootObj.contains(names_list[i])){

            QJsonObject temp_obj = rootObj[names_list[i]].toObject();
            QJsonArray temp_arr = temp_obj["recipe"].toArray();

            QString final_str = names_list[i];
            for (int i = 0; i < temp_arr.count(); i++){
                final_str += "\n       => "+ temp_arr[i].toString();

            }

            if (!global_recipe_list.contains(final_str)){
                global_recipe_list.append(final_str);
            }

        }
    }


    recipie_Dialog *sh_dlg = new recipie_Dialog(this);
    sh_dlg->show();



















//    global_ingred_map.clear();
//    global_recipe_list.clear();

//    QString text_string = ui->names_plainTextEdit->toPlainText();
   // qDebug() << text_string;

//    QStringList user_list = text_string.split('\n');
//    user_list.sort();
   // qDebug() << "list of items: "<< user_list;


//    QMap<QString,int> item_count;

//    for (int i = 0; i < user_list.size(); i++){
//        int count_val = user_list.count(user_list[i]);
//        item_count.insert(user_list[i],count_val);

//    }

//    qDebug() << "item count: "<< item_count;

//    user_list.removeDuplicates();

    //qDebug() << "After removing duplicates: "<<user_list ;





//    QStringList food_list = rootObj.keys();
   // qDebug() << "food List: " << food_list;
   // int food_count = rootObj.count();
    //qDebug() << food_count;


//    for (int i = 0;i<user_list.size(); i++){
//        if(food_list.contains(user_list[i])){

//            int food_count = item_count.value(user_list[i]);
//            QString temp_name = user_list[i];
//            QJsonObject temp_food = rootObj[temp_name].toObject();


//            QStringList temp_ingred_list = temp_food.keys();

//            int ingred_count = temp_ingred_list.count();



//            for (int i = 0; i < ingred_count; i++){
//                if(temp_ingred_list[i]!="recipe"){


//                    QString ingred_str = temp_food[temp_ingred_list[i]].toString();

//                    QRegularExpression unit_re;
//                    unit_re.setPattern("[a-z]+");

//                    QRegularExpressionMatch unit_match = unit_re.match(ingred_str);
//                    if (unit_match.hasMatch()){
//                        QString unit_matched = unit_match.captured(0);
//                        qDebug() <<"Found match: " << unit_matched;
//                    }else{
//                        qDebug() << "Failed" ;
//                    }

//                    QRegularExpression num_re;
//                    num_re.setPattern("[0-9]+[.][0-9]+");

//                    QRegularExpressionMatch num_match = num_re.match(ingred_str);
//                    if (num_match.hasMatch()){
//                        QString num_matched = num_match.captured(0);
//                        float num_val = num_matched.toFloat();
//                        num_val = num_val*food_count;

//                        if(global_ingred_map.contains(temp_ingred_list[i])){
//                            num_val = global_ingred_map.value(temp_ingred_list[i])+num_val;
//                        }

//                        global_ingred_map.insert(temp_ingred_list[i],num_val);

//                        qDebug() <<"Found match: " << num_val;

//                    }else{
//                        qDebug() << "Failed" ;
//                    }





//                }
//            }






//            QJsonArray new_arr =  temp_food["recipe"].toArray();
//            QString final_str = user_list[i];

//            for (int i = 0; i < new_arr.count();i++){
//                final_str += "\n       => "+ new_arr[i].toString();

//            }

//            if (!global_recipe_list.contains(final_str)){
//                global_recipe_list.append(final_str);
//            }

//        }
//    }


//    qDebug() << "Final map" << global_ingred_map;


//   recipie_Dialog *sh_dlg = new recipie_Dialog(this);
//   sh_dlg->show();



//   std::string input = my_str.toStdString();
//   std::smatch match_res;
//   std::regex pattern("[0-9]+[.][0-9]+");



//   if (std::regex_search(input,match_res,pattern)){
//       double num_val = std::stod(match_res[0]);
//       qDebug() << "Found: " << num_val;
//   }else{
//       qDebug() << "Failed";
//   }









/*
    for(int i = 0 ; i < food_count; i++){

        QString food_name = food_list[i];
        //qDebug() << food_name;
        QJsonObject current_food = rootObj[food_name].toObject();
        //qDebug() << current_food;

        QStringList ingredients_list = current_food.keys();
        qDebug() << ingredients_list;

        int ingredients_count = current_food.count();
        qDebug() << ingredients_count;

        QString item_name = ingredients_list[0];
        qDebug() << "Boolean value" << current_food.contains(item_name);
    }
*/



}

void cook_Dialog::on_ing_pushButton_clicked()
{

    QString names_str = ui->names_plainTextEdit->toPlainText();
    QStringList names_list = names_str.split("\n");

    names_list.sort();
    qDebug() << "list of items: "<< names_list;


    QMap<QString,int> item_count;


    for (int i = 0; i < names_list.count(); i++){
        int count_val = names_list.count(names_list[i]);
        item_count.insert(names_list[i],count_val);

    }

//    qDebug() << "item count: "<< item_count;

    names_list.removeDuplicates();

//    qDebug() << "After removing duplicates: "<<names_list ;

//    QString str = "  2.0   pcs   ";
//    QStringList lst = str.split(" ");
//    lst.removeAll("");

//    qDebug() << lst;

    QMap<QString,float> ingred_map_count;

    for (int i = 0; i < names_list.count(); i++ ){

        int multiplier = item_count.value(names_list[i]);

        QJsonObject ing_temp_obj = rootObj[names_list[i]].toObject();
        QStringList ing_temp_list = ing_temp_obj.keys();
        int ingred_count = ing_temp_obj.keys().count();

        for(int i = 0; i < ingred_count; i++){
            if(ing_temp_list[i]!="recipe"){

                QStringList temp_lst = ing_temp_obj[ing_temp_list[i]].toString().split(" ");
                temp_lst.removeAll("");

                QString truncate_str = ing_temp_list[i]+"---"+temp_lst[1];
                float quant = temp_lst[0].toFloat();
                quant = quant * multiplier;

                if(ingred_map_count.contains(truncate_str)){
                    float updated_num = ingred_map_count.value(truncate_str)+quant;
                    ingred_map_count.insert(truncate_str,updated_num);


                }else{
                    ingred_map_count.insert(truncate_str,quant);

                }


            }
        }

    }

    qDebug() << ingred_map_count;


    QString final_disp_str;
    QStringList map_count_lst = ingred_map_count.keys();

    for(int i = 0; i < map_count_lst.count(); i++){

        QStringList divided_lst = map_count_lst[i].split("---");
        QString conv_num = QString::number(ingred_map_count.value(map_count_lst[i]));


        if(i==map_count_lst.count()-1){
            final_disp_str += divided_lst[0]+": "+conv_num+" "+divided_lst[1];
        }else{
            final_disp_str += divided_lst[0]+": "+conv_num+" "+divided_lst[1]+"\n";

        }


    }

    qDebug() << final_disp_str;

    final_ingred_disp = final_disp_str;

    ingred_Dialog *ing_dlg = new ingred_Dialog(this);
    ing_dlg->show();


    //    }



    //    QRegularExpression unit_re;
    //    unit_re.setPattern("[a-z]+");

    //    QRegularExpressionMatch unit_match = unit_re.match(ingred_str);
    //    if (unit_match.hasMatch()){
    //        QString unit_matched = unit_match.captured(0);
    //        qDebug() <<"Found match: " << unit_matched;
    //    }else{
    //        qDebug() << "Failed" ;
    //    }

    //    QRegularExpression num_re;
    //    num_re.setPattern("[0-9]+[.][0-9]+");

    //    QRegularExpressionMatch num_match = num_re.match(ingred_str);
    //    if (num_match.hasMatch()){
    //        QString num_matched = num_match.captured(0);
    //        float num_val = num_matched.toFloat();
    //        num_val = num_val*food_count;

    //        if(global_ingred_map.contains(temp_ingred_list[i])){
    //            num_val = global_ingred_map.value(temp_ingred_list[i])+num_val;
    //        }

    //        global_ingred_map.insert(temp_ingred_list[i],num_val);

    //        qDebug() <<"Found match: " << num_val;

    //    }else{
    //        qDebug() << "Failed" ;
    //    }





}
